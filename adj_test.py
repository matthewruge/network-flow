from adjacency import *

g = Graph()

g.add_edge('A', 'B',  5)
g.add_edge('B', 'C',  8)
g.add_edge('C', 'D',  2)
g.add_edge('A', 'E',  6)
g.add_edge('E', 'F', 14)
g.add_edge('F', 'D', 13)
g.add_edge('B', 'F',  3)

flow = g.flow('A', 'D')
g.dot(flow=flow, output='simple.gv')
# g.draw(flow=flow)

