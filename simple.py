from graph import *


g = graph()
g.add_edge('A', 'B', 1)
g.add_edge('B', 'C', 2)
g.add_edge('A', 'C', 3)
g.flow('A', 'C')
g.draw()