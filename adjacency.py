import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt
from ortools.graph import pywrapgraph
from jinja2 import Template


class Node:

    def __init__(self, label):
        self.label = label
        self.id = id(self)
        self.neighbors = {}
    
    def __eq__(self, other):
        if isinstance(other, str) or isinstance(other, int):
            return any(
                (other == self.label,
                other == self.id)
            )
        elif isinstance(other, Node):
            return id(other) == id(self)

    def normalize(self):
        total = sum([
            n[1]['capacity'] for n in self.neighbors.items()
        ])
        print(f"{self.label}: {total}")
            
        for n_key in self.neighbors:
            neighbor = self.neighbors[n_key]
            if total == 0:
                neighbor['norm'] = 0
            else:
                neighbor['norm'] = neighbor['capacity']/total

    def add_neighbor(self, n, c):
        if id(n) not in self.neighbors:
            self.neighbors[id(n)] = {
                'node': n,
                'capacity': c
            }
        else:
            print(f"{n.label} already a neighbor of {self.label}")
        self.normalize()


class Graph:

    def __init__(self):
        self.nodes = []

    def get_node(self, n, create=False):
        if n in self.nodes:
            return self.nodes[self.nodes.index(n)]
        elif create:
            if isinstance(n, Node):
                self.nodes += [n]
            elif isinstance(n, str):
                n = Node(n)
                self.nodes += [n]
            return n
        else:
            return None

    def add_edge(self, a, b, c):
        a = self.get_node(a, create=True)
        b = self.get_node(b, create=True)
        a.add_neighbor(b, c)

    def dot(self, flow=[], output='out.gv'):
        with open('graphviz_template.j2') as f:
            template = Template(f.read())
        with open(output, 'w') as f:
            f.write(
                template.render(
                    nodes=self.nodes,
                    flow=flow
                )
            )

    def draw(self, flow=[]):
        G = nx.MultiDiGraph(directed=True)
        edge_labels = {}
        colors = []
        print(flow)
        for node in self.nodes:
            for neighbor_id in node.neighbors.keys():
                neighbor = node.neighbors[neighbor_id]
                G.add_edge(id(node),neighbor_id)
                colors+=['black']
                edge_labels[(id(node),neighbor_id)] = f"{neighbor['capacity']}"
        for e in flow:
            print(f"{self.nodes[e[0]].label}->{self.nodes[e[1]].label}")
            G.add_edge(self.nodes[e[0]].id,self.nodes[e[1]].id)
            colors += ['red']
        print(len(G.edges))
        print(len(colors))
        # for e in G.edges:
        #     a_i = self.node_index(e[0])
        #     b_i = self.node_index(e[1])
        #     if (a_i,b_i) in flow:
        #         colors+=['red']
        #     else:
        #         colors+=['black']

        pos = graphviz_layout(G,prog="dot")
        options = {
            'node_size': 500,
            'width': 3,
            'arrowstyle': '-|>',
            'arrowsize': 12,
            'arrows': True,
            'with_labels': False,
            'edge_color': colors
        }
        nx.draw_networkx(
            G, pos, **options
        )
        nx.draw_networkx_edge_labels(
            G, pos,
            edge_labels=edge_labels,
            font_color='red'
        )
        nx.draw_networkx_labels(
            G,pos,
            labels={id(n):f"{n.label}{self.nodes.index(n)}" for n in self.nodes}
        )
        plt.show()

    def node_index(self, name):
        n = self.get_node(name)
        if n:
            return self.nodes.index(n)
        else:
            return None

    def flow(self, frm, to):
        src = self.node_index(frm)
        sink = self.node_index(to)
        max_flow = pywrapgraph.SimpleMaxFlow()
        for node in self.nodes:
            for neighbor_key in node.neighbors:
                neighbor = node.neighbors[neighbor_key]
                max_flow.AddArcWithCapacity(
                    self.node_index(node),
                    self.node_index(neighbor['node']),
                    neighbor['capacity'])
        max_flow.Solve(src, sink)
        flow = []
        print('Max flow:', max_flow.OptimalFlow())
        print('------------------------')
        print('  Arc    Flow / Capacity')
        for i in range(max_flow.NumArcs()):
            print('%1s -> %1s   %3s  / %3s' % (
            max_flow.Tail(i),
            max_flow.Head(i),
            max_flow.Flow(i),
            max_flow.Capacity(i)))
            print(f"flow-size: {max_flow.Flow(i)}")
            if max_flow.Flow(i) > 0:
                flow += [(max_flow.Tail(i),max_flow.Head(i),max_flow.Flow(i))]
        return flow