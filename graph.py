from numpy import isin
from ortools.graph import pywrapgraph

import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt


class graph:
    def __init__(self):
        self.nodes = []
        self.edges = []

    def get_node(self, name, create=False):
        if isinstance(name, node):
            return name
        for n in self.nodes:
            if n.label == name:
                return n
        if create:
            n = node(name)
            self.nodes += [n]
            return n
        else:
            return None

    def node_index(self, name):
        n = self.get_node(name)
        if n:
            return self.nodes.index(n)
        else:
            return None
        
    def get_edge(self, a, b, capacity, create=False):
        a = self.get_node(a, create=create)
        b = self.get_node(b, create=create)
        # print(f"!: {a},{b}")
        for e in self.edges:
            # print(f"?: {e.start},{e.end}")
            if e.start == a and e.end == b and e.capacity == capacity:
                # print(f"-> {e.start},{e.end}")
                return e
        if create:    
            e = edge(self.get_node(a, create=create), self.get_node(b, create=create), capacity)
            self.edges += [e]
            return e
        else:
            return None

    def add_edge(self, a, b, capacity):
        return self.get_edge(a, b, capacity, create=True)

    def add_node(self, a):
        return self.get_node(a, create=True)

    def draw(self):
        G = nx.DiGraph(directed=True)
        G.add_edges_from(
            [(e.a,e.b) for e in self.edges]
        )
        pos = graphviz_layout(G)
        options = {
            'node_size': 500,
            'width': 3,
            'arrowstyle': '-|>',
            'arrowsize': 12,
            'arrows': True,
            'with_labels': True,
        }
        nx.draw_networkx(G, pos, **options)

        nx.draw_networkx_edge_labels(
            G, pos,
            edge_labels={(e.a, e.b): f"{e.flow}/{e.capacity}" for e in self.edges},
            font_color='red'
        )

        plt.show()

    def flow(self, frm, to):
        src = self.node_index(frm)
        sink = self.node_index(to)
        print(f"from {frm} to {to}")
        max_flow = pywrapgraph.SimpleMaxFlow()
        for e in self.edges:
            max_flow.AddArcWithCapacity(
                self.node_index(e.a),
                self.node_index(e.b),
                e.capacity)
        max_flow.Solve(src, sink)
        print('Max flow:', max_flow.OptimalFlow())
        print('------------------------')
        print('  Arc    Flow / Capacity')
        for i in range(max_flow.NumArcs()):
            e = self.get_edge(
                self.nodes[max_flow.Tail(i)],
                self.nodes[max_flow.Head(i)],
                max_flow.Capacity(i)
            )
            e.flow = max_flow.Flow(i)
            print('%1s -> %1s   %3s  / %3s' % (
            max_flow.Tail(i),
            max_flow.Head(i),
            max_flow.Flow(i),
            max_flow.Capacity(i)))

class node:
    def __init__(self, label):
        self.label = label 
    def __eq__(self, other):
        if isinstance(other, node):
            return self.label == other.label 
        return False

class edge:
    def __init__(self, start, end, capacity):
        self.start = start
        self.end = end
        self.capacity = capacity
        self.flow = None
    @property
    def a(self):
        return self.start.label
    @property
    def b(self):
        return self.end.label

    @classmethod
    def bylabel(self, a, b, nodes, capacity):
        start,end = None, None 
        for n in nodes:
            if start and end:
                break
            elif n.label == a:
                start = n 
            elif n.label == b:
                end = n
        return self(start, end, capacity)